package br.com.mastertech.consultaUsuarioOauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultaUsuarioOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaUsuarioOauthApplication.class, args);
	}

}
