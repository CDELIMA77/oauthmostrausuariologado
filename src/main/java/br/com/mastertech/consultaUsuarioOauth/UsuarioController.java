package br.com.mastertech.consultaUsuarioOauth;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @PostMapping
    public Usuario create(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}
